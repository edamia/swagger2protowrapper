package swagger2proto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import swagger2proto.model.proto.Proto;
import swagger2proto.model.proto.ProtoField;
import swagger2proto.model.proto.ProtoMessage;
import swagger2proto.model.proto.ProtoRPC;
import swagger2proto.model.proto.ProtoService;

public class ProtoWriter {
	
	public static String generateProtoFileContent(Proto proto){
		String content = "";
		
		content += "syntax = \"proto3\"";
		content += System.lineSeparator();
		content += System.lineSeparator();
		
		// Write Services
		for (ProtoService service : proto.getServices()) {
			content += "Service " + service.getName() + " {";
			content += System.lineSeparator();
			//TODO WRITE RPCs
			for(ProtoRPC rpcMessage: service.getRpcs()){
				content += "  rpc " + rpcMessage.getName();
				content += "(" + rpcMessage.getRequestMessage().getName() + ")";
				content += " returns (" + rpcMessage.getResponseMessage().getName() + ")";
				content += System.lineSeparator();
			}
			content += "}";
			content += System.lineSeparator();
		}
		// Write messages
		for(ProtoMessage message : proto.getMessages()){
			content += "message " + message.getName() + " {";
			content += System.lineSeparator();
			for(ProtoField field : message.getFields()){
				content += "  ";
				if(field.isRepeated()){
					content += "repeated ";
				}
				content += field.getType() + " " + field.getName() + " = " + field.getID() + ";";
				content += System.lineSeparator();
			}
			content += "}";
			content += System.lineSeparator();
		}
		return content;
	}
	
	// Write into output file
	public static void writeProtoFile(Proto proto, String outputPath) throws IOException{
		String fileContent = generateProtoFileContent(proto);
		writeFile(new File(outputPath), fileContent);
	}
	
	public static void writeFile(File outputFile, String content) throws IOException{
		if (!outputFile.exists()) {
			outputFile.getParentFile().mkdirs();
			outputFile.createNewFile();
		}
		FileWriter fw = new FileWriter(outputFile, false);
		fw.write(content);
		fw.close();
	}
}
