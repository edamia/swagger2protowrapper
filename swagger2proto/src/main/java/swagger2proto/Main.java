package swagger2proto;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

import io.swagger.models.Swagger;
import io.swagger.parser.SwaggerParser;
import swagger2proto.model.proto.Proto;

public class Main {
	
	public static void main(String[] args) {
		String inputFilePath = null;
		String outputFilePath = null;
		
		if(args.length == 0){
			// Open a File Chooser Dialog to select the input file
			JFileChooser jfc = new JFileChooser();
			jfc.setFileSelectionMode(JFileChooser.FILES_ONLY);
			jfc.setFileFilter(new FileNameExtensionFilter("Swagger definitions (json, yaml)", "yaml", "json"));
			int result = 0;
			result = jfc.showOpenDialog(null);
			if (result == JFileChooser.APPROVE_OPTION && jfc.getSelectedFile() != null){
				inputFilePath = jfc.getSelectedFile().getAbsolutePath();
				File output = new File(inputFilePath);
				String fileExtension = ".proto";
				String outputFileName = output.getName();
				int pos = outputFileName.lastIndexOf(".");
				if (pos > 0) {
					outputFileName = outputFileName.substring(0, pos);
				}
				outputFilePath = output.getParent() + File.separatorChar + outputFileName + fileExtension;
				System.out.println("Input: " + inputFilePath);
				System.out.println("Output: " + outputFilePath);
			}
		}
		// Verify if arguments have been passed
		else if(args.length >= 1 && args.length <= 2){
			try{
				inputFilePath = args[0];
			}catch(ArrayIndexOutOfBoundsException e){
				inputFilePath = null;
			}
			
			try{
				outputFilePath = args[1];
			}catch(ArrayIndexOutOfBoundsException e){
				outputFilePath = null;
			}
		}else{
			System.out.println("ERROR: Invalid number of arguments ("+args.length+")!");
			System.out.println("Usage:");
			System.out.println("swagger2proto input_swagger_file [output_proto_file]");
			System.out.println("\tinput_swagger_file - this must be a swagger definition file which will be converted into a .proto definition file");
			System.out.println("\toutput_proto_file [optional] - this is the file to which the .proto definition will be written. If no output file is specified, a new file will be created at the input file location");
			return;
		}
		
		//Parse Swagger Input File
		File inputFile = new File(inputFilePath);
		if(inputFile.exists() && inputFile.isFile() && inputFile.canRead()){
			Swagger swagger = new SwaggerParser().read(inputFile.getAbsolutePath());
			if(swagger != null){
				System.out.println("Successfully parsed Swagger file");
				Proto proto = Proto.parse(swagger);
				System.out.println("Successfully parsed Swagger File into Proto");
				try {
					ProtoWriter.writeProtoFile(proto, outputFilePath);
					System.out.println("Successfully wrote .proto file");
				} catch (IOException e) {
					System.err.println("ERROR: Could not write .proto file");
					e.printStackTrace();
				}
			}else{
				System.err.println("ERROR: Could not parse input swagger file.");
			}
		}else{
			System.err.println("ERROR: The input file path is invalid (e.g. does not exist/is not a file/cannot read)\nSpecified path: "+inputFilePath);
		}
	}
	
}
