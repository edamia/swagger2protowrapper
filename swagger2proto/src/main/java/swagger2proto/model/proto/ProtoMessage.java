package swagger2proto.model.proto;

import java.util.ArrayList;

/**
 * Describes Proto Messages
 */
public class ProtoMessage {

	private String name = "MessageName";
	private ArrayList<ProtoField> fields = new ArrayList<ProtoField>();
	
	public ProtoMessage(){}
	
	public ProtoMessage(String name, ArrayList<ProtoField> fields) {
		super();
		this.name = name;
		this.fields = fields;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<ProtoField> getFields() {
		return fields;
	}

	public void setFields(ArrayList<ProtoField> fields) {
		this.fields = fields;
	}
	
	
}
