package swagger2proto.model.proto;


/**
 * Describes Proto Message Fields
 */
public class ProtoField {
	
	private boolean repeated = false;
	private String type = ProtoType.string;
	private String name = "";
	private int ID = 1;
	
	public ProtoField(){};
	
	public ProtoField(boolean repeated, String type, String name, int iD) {
		super();
		this.type = type;
		this.setRepeated(repeated);
		this.name = name;
		ID = iD;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}

	public boolean isRepeated() {
		return repeated;
	}

	public void setRepeated(boolean repeated) {
		this.repeated = repeated;
	}
	
	
}
