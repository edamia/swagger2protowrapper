package swagger2proto.model.proto;

/**
 * Describes Proto RPC Methods
 */
public class ProtoRPC {
	private String name = "RPCName";
	private ProtoMessage requestMessage = null;
	private ProtoMessage responseMessage = null;
	
	public ProtoRPC(){}
	
	public ProtoRPC(String name, ProtoMessage requestMessage, ProtoMessage responseMessage) {
		super();
		this.name = name;
		this.requestMessage = requestMessage;
		this.responseMessage = responseMessage;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ProtoMessage getRequestMessage() {
		return requestMessage;
	}
	public void setRequestMessage(ProtoMessage requestMessage) {
		this.requestMessage = requestMessage;
	}
	public ProtoMessage getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(ProtoMessage responseMessage) {
		this.responseMessage = responseMessage;
	}
	
	
}
