package swagger2proto.model.proto;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map.Entry;

import io.swagger.models.ArrayModel;
import io.swagger.models.Model;
import io.swagger.models.Operation;
import io.swagger.models.Path;
import io.swagger.models.RefModel;
import io.swagger.models.Response;
import io.swagger.models.Swagger;
import io.swagger.models.parameters.BodyParameter;
import io.swagger.models.parameters.FormParameter;
import io.swagger.models.parameters.HeaderParameter;
import io.swagger.models.parameters.Parameter;
import io.swagger.models.parameters.PathParameter;
import io.swagger.models.parameters.QueryParameter;
import io.swagger.models.properties.ArrayProperty;
import io.swagger.models.properties.Property;
import io.swagger.models.properties.RefProperty;
import io.swagger.models.properties.StringProperty;
import swagger2proto.ProtoWriter;

/**
 * Parses an OpenAPI Specification into a Proto File Description
 */
/**
 * @author edyta
 *
 */
/**
 * @author edyta
 *
 */
public class Proto {

	private ArrayList<ProtoMessage> messages = new ArrayList<ProtoMessage>();
	private ArrayList<ProtoService> services = new ArrayList<ProtoService>();

	public Proto() {
	};

	public Proto(ArrayList<ProtoMessage> messages, ArrayList<ProtoService> services) {
		super();
		this.messages = messages;
		this.services = services;
	}

	public ArrayList<ProtoMessage> getMessages() {
		return messages;
	}

	public void setMessages(ArrayList<ProtoMessage> messages) {
		this.messages = messages;
	}

	public ArrayList<ProtoService> getServices() {
		return services;
	}

	public void setServices(ArrayList<ProtoService> services) {
		this.services = services;
	}

	public static Proto parse(Swagger swagger) {
		Proto proto = new Proto();
		
		// RPC Services
		ProtoService service = new ProtoService();
		String serviceName = swagger.getInfo().getTitle();
		serviceName = serviceName.replace(" ", "");
		service.setName(serviceName);
		proto.getServices().add(service);

		// MESSAGES FOR SWAGGER DEFINITION OBJECTS
		for (Entry<String, Model> entry : swagger.getDefinitions().entrySet()) {
			ProtoMessage message = new ProtoMessage();
			String messageName = entry.getKey();
			message.setName(messageName);

			Model model = entry.getValue();
			
			parseModelToMessageFields(message, model);

			proto.getMessages().add(message);
		}

		for (Path path : swagger.getPaths().values()) {
			for (Operation operation : path.getOperationMap().values()) {
				String rpcName = operation.getOperationId();

				// Get/Generate request messages from parameters
				ProtoMessage requestMessage = new ProtoMessage();
				requestMessage.setName(rpcName + "Request");
				int id = 1;
				for (Parameter operationParameter : operation.getParameters()) {
					
					if (operationParameter instanceof BodyParameter) {
						BodyParameter parameter = (BodyParameter) operationParameter;
						Model model = parameter.getSchema();
						if (model instanceof RefModel) {
							String ref = model.getReference();
							String messageName = ref.substring(ref.lastIndexOf('/') + 1);
							for (ProtoMessage message : proto.getMessages()) {
								if (message.getName().equalsIgnoreCase(messageName)) {
									requestMessage.getFields().add(
											new ProtoField(false, message.getName(), messageName.toLowerCase(), id));
									break;
								}
							}
						} else if (model instanceof ArrayModel) {
							ArrayModel array = (ArrayModel) model;
							RefProperty item = (RefProperty) array.getItems();
							if (item instanceof RefProperty) {
								String refItem = item.get$ref();
								String itemName = refItem.substring(refItem.lastIndexOf('/') + 1);
								for (ProtoMessage message : proto.getMessages()) {
									if (message.getName().equalsIgnoreCase(itemName)) {
										requestMessage.getFields().add(
												new ProtoField(true, message.getName(), itemName.toLowerCase(), id));
										break;
									}
								}
							} else {
								Property primitiveItem = array.getItems();
								String itemType = primitiveItem.getType();
								String itemFormat = primitiveItem.getFormat();
								String primItemParsed = ProtoType.parseType(itemType, itemFormat);
								requestMessage.getFields()
										.add(new ProtoField(true, primItemParsed, parameter.getName(), id));
							}
						} else if (model instanceof StringProperty) {
							System.out.println("model: " + model.getClass());
							StringProperty strProperty = (StringProperty) model;
							String itemProp = strProperty.getType();
							requestMessage.getFields().add(new ProtoField(false, itemProp, parameter.getName(), id));
						} else {
							System.out.println("UNIMPLEMENTED: " + model.getClass());
						}
						// Query parameters
					} else if (operationParameter instanceof QueryParameter) {
						QueryParameter parameter = (QueryParameter) operationParameter;
						String queryType = parameter.getType();
						String queryFormat = parameter.getFormat();
						if (queryType == "array") {
							Property queryItem = parameter.getItems();
							String arrayItemType = queryItem.getType();
							String itemType = ProtoType.parseType(arrayItemType, null);
							requestMessage.getFields()
									.add(new ProtoField(true, itemType, parameter.getName(), id));
						} else {
							String type = ProtoType.parseType(queryType, queryFormat);
							requestMessage.getFields().add(new ProtoField(false, type, parameter.getName(), id));
						}
						// Path parameters
					} else if (operationParameter instanceof PathParameter) {
						PathParameter parameter = (PathParameter) operationParameter;
						String operationType = parameter.getType();
						if (operationType == "array") {
							Property operationItem = parameter.getItems();
							String arrayItemType = operationItem.getType();
							String itemType = ProtoType.parseType(arrayItemType, null);
							requestMessage.getFields()
									.add(new ProtoField(true, itemType, parameter.getName(), id));
						}else{ 
							String type = ProtoType.parseType(parameter.getType(), parameter.getFormat());
							requestMessage.getFields().add(new ProtoField(false, type, parameter.getName(), id));
						}
						// Form Parameters
					} else if (operationParameter instanceof FormParameter) {
						FormParameter parameter = (FormParameter) operationParameter;
						String formType = parameter.getType();
						if (formType == "array") {
							Property formItem = parameter.getItems();
							String arrayItemType = formItem.getType();
							String itemType = ProtoType.parseType(arrayItemType, null);
							requestMessage.getFields()
									.add(new ProtoField(true, itemType, parameter.getName(), id));
						} else {
							String type = ProtoType.parseType(parameter.getType(), parameter.getFormat());
							requestMessage.getFields().add(new ProtoField(false, type, parameter.getName(), id));
							}
						// Header Parameters
					}else if (operationParameter instanceof HeaderParameter) {
						HeaderParameter parameter = (HeaderParameter) operationParameter;
						String headerType = parameter.getType();
						if (headerType == "array") {
							Property headerItem = parameter.getItems();
							String arrayItemType = headerItem.getType();
							String itemType = ProtoType.parseType(arrayItemType, null);
							requestMessage.getFields()
									.add(new ProtoField(true, itemType, parameter.getName(), id));
						}else{
							String type = ProtoType.parseType(parameter.getType(), parameter.getFormat());
							requestMessage.getFields().add(new ProtoField(false, type, parameter.getName(), id));
						}
					}else {
						System.out.println("Unimplemented Parameter type: " + operationParameter.getIn());
					}
					
					id++;
				}

				// Get/Generate request messages from response objects
				ProtoMessage responseMessage = new ProtoMessage();
				responseMessage.setName(rpcName + "Response");
				id = 1;
				for (Entry<String, Response> operationResponse : operation.getResponses().entrySet()) {
					String respName = operationResponse.getKey(); // e.g. "200"
					Response response = operationResponse.getValue(); // e.g. "description"
					if (response.getSchema() != null) {
						Property schema = response.getSchema();
						responseMessage.getFields().add(parsePropertyToField(schema, respName, id));
					} else {
						String respType = ProtoType.string;
						responseMessage.getFields().add(new ProtoField(false, respType, respName, id));
					}
					id++;
				}

				proto.getMessages().add(requestMessage);
				proto.getMessages().add(responseMessage);

				ProtoRPC rpc = new ProtoRPC();
				rpc.setName(rpcName);
				rpc.setRequestMessage(requestMessage);
				rpc.setResponseMessage(responseMessage);
				service.getRpcs().add(rpc);
			}
		}

		return proto;
	}

	/**
	 * Parses the given model and adds fields to the given ProtoMessage object
	 * 
	 * @param message
	 * @param model
	 */
	public static void parseModelToMessageFields(ProtoMessage message, Model model) {
		int id = 1;

		for (Entry<String, Property> entryProperty : model.getProperties().entrySet()) {
			message.getFields().add(parsePropertyToField(entryProperty.getValue(), entryProperty.getKey(), id));
			id++;
		}
	}

	public static ProtoField parsePropertyToField(Property property, String fieldName, int id) {
		if (property instanceof RefProperty) {
			RefProperty defRef = (RefProperty) property;
			String ref = defRef.get$ref();
			ref = ref.substring(ref.lastIndexOf('/') + 1);
			return new ProtoField(false, ref, ref.toLowerCase(), id);
		} else if (property instanceof ArrayProperty) {
			ArrayProperty arrayProperty = (ArrayProperty) property;
			Property arrayItem = arrayProperty.getItems();
			if (arrayItem instanceof RefProperty) {
				RefProperty defRef = (RefProperty) arrayItem;
				String ref = defRef.get$ref();
				ref = ref.substring(ref.lastIndexOf('/') + 1);
				return new ProtoField(true, ref, ref.toLowerCase(), id);
			} else {
				String itemType = arrayItem.getType();
				String itemFormat = arrayItem.getFormat();
				String primItemParsed = ProtoType.parseType(itemType, itemFormat);
				return new ProtoField(true, primItemParsed, fieldName, id);
			}
		} else {
			String type = property.getType();
			String format = property.getFormat();
			String protoType = ProtoType.parseType(type, format);
			if (protoType.equalsIgnoreCase("null")) {
				System.out.println("UNIMPLEMENTED: " + property.getClass());
			}
			return new ProtoField(false, protoType, fieldName, id);
		}
	}

	public static void convertToProtoFile(Swagger swagger, String outputPath) throws IOException {
		Proto proto = parse(swagger);
		ProtoWriter.writeProtoFile(proto, outputPath);
	}
}
