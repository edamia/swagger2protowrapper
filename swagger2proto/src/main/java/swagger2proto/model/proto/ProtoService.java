package swagger2proto.model.proto;

import java.util.ArrayList;

/**
 * Describes Proto Services
 */
public class ProtoService {

	private String name = "ServiceName";
	private ArrayList<ProtoRPC> rpcs = new ArrayList<ProtoRPC>();
	
	public ProtoService(){}
	
	public ProtoService(String name, ArrayList<ProtoRPC> rpcs) {
		super();
		this.name = name;
		this.rpcs = rpcs;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<ProtoRPC> getRpcs() {
		return rpcs;
	}
	public void setRpcs(ArrayList<ProtoRPC> rpcs) {
		this.rpcs = rpcs;
	}
	
}
