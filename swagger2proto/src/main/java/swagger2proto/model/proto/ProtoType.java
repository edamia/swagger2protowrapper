package swagger2proto.model.proto;

/**
 * Mapping of Swagger data types to proto data types
 */
public class ProtoType {
	public static final String 
			proto_double = "double", 
			proto_float = "float", 
			int32 = "int32", 
			int64="int64", 
			uint32="uint32",
			uint64="uint64", 
			sint32="sint32",
			sint64="sint64", 
			fixed32="fixed32",
			fixed64="fixed64", 
			sfixed32="sfixed32", 
			sfixed64="sfixed64", 
			bool="bool",
			string="string", 
			bytes="bytes",
			repeated="repeated";

	public static String parseType(String type, String format) {
		String protoType = "null";
		if (type.equalsIgnoreCase("integer")) {
			protoType = ProtoType.int32;
			if (format != null && format.equalsIgnoreCase("int32")) {
				protoType = ProtoType.int32;
			} else if (format != null && format.equalsIgnoreCase("int64")) {
				protoType = ProtoType.int64;
			}
		} else if (type.equalsIgnoreCase("number")) {
			if (format != null && format.equalsIgnoreCase("float")) {
				protoType = ProtoType.proto_float;
			} else if (format != null && format.equalsIgnoreCase("double")) {
				protoType = ProtoType.proto_double;
			}
		} else if (type.equalsIgnoreCase("string")) {
			protoType = ProtoType.string;
			if (format != null && format.equalsIgnoreCase("binary")) {
				protoType = ProtoType.fixed64;
			}else if (format != null && format.equalsIgnoreCase("byte")) {
				protoType = ProtoType.bytes;
			}
		} else if (type.equalsIgnoreCase("boolean")) {
			protoType = ProtoType.bool;
		}
		return protoType;
	}
}
